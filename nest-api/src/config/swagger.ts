import { DocumentBuilder } from '@nestjs/swagger';

export const swagger = new DocumentBuilder()
  .setTitle('Todos API')
  .setDescription('The Todos API description')
  .setVersion('1.0')
  .addTag('todos')
  .build();
