import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Logger, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiNoContentResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags, getSchemaPath } from '@nestjs/swagger';
import { TodosService } from './todos.service';
import { Todo } from '../data/entities/todo.entity';
import { TodoDto } from '../data/dto/todo.dto';

@Controller('api/todos')
@ApiTags('todos')
export class TodosController {
  private readonly logger: Logger;

  constructor(private readonly todoService: TodosService) {
    this.logger = new Logger(TodosController.name);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Fetch all todo items' })
  @ApiOkResponse({
    status: HttpStatus.OK,
    description: 'Return all todo items',
    schema: {
      allOf: [
        { type: 'array', items: { $ref: getSchemaPath(Todo) } }
      ]
    }
  })
  async fetch(): Promise<Todo[]> {
    const todos = await this.todoService.fetch();
    this.logger.log('Fetch all todo items');
    return todos;
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Find a todo item' })
  @ApiParam({ name: 'id', type: Number })
  @ApiOkResponse({
    status: HttpStatus.OK,
    description: 'Return a specify todo item',
    type: Todo
  })
  async find(@Param('id') id: number): Promise<Todo | null> {
    const todo = await this.todoService.find(id);
    this.logger.log(`Find a todo item by id: ${id}`);
    return todo;
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Create a todo item' })
  @ApiBody({ description: 'Todo item request body', type: TodoDto })
  @ApiCreatedResponse({
    status: HttpStatus.CREATED,
    description: 'Return a new todo item',
    type: Todo
  })
  async create(@Body() newTodo: TodoDto): Promise<Todo> {
    const todo = await this.todoService.create(newTodo);
    this.logger.log('Create a todo item');
    return todo;
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Update a todo item' })
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ description: 'Todo item request body', type: TodoDto })
  @ApiOkResponse({
    status: HttpStatus.OK,
    description: 'Return a updated todo item',
    type: Todo
  })
  async update(@Param('id') id: number, @Body() updatedTodo: TodoDto): Promise<Todo | null> {
    const todo = await this.todoService.update(id, updatedTodo);
    this.logger.log(`Update a todo item by id: ${id}`);
    return todo;
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiOperation({ summary: 'Delete a todo item' })
  @ApiParam({ name: 'id', type: Number })
  @ApiNoContentResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The todo items was deleted.'
  })
  async remove(@Param('id') id: number): Promise<void> {
    await this.todoService.remove(id);
    this.logger.log(`Delete a todo item by id: ${id}`);
  }
}
