import { ApiProperty } from '@nestjs/swagger';

export class TodoDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  date: Date;

  @ApiProperty()
  isDone: boolean;
}
