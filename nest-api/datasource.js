var typeOrm = require('typeorm');
require('dotenv').config();

var datasource = new typeOrm.DataSource({
  type: 'postgres',
  host: process.env.DB_HOST,
  port: 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: 'nest',
  synchronize: false,
  logging: false,
  entities: ['dist/data/entities/**/*.js'],
  migrations: ['dist/data/migrations/**/*.js'],
  cli: {
    entitiesDir: 'dist/data/entities',
    migrationsDir: 'dist/data/migrations',
  }
});

exports.default = datasource;
