# Learn NestJS

- การใช้งาน NestJS 9 Framework
- การใช้งาน class component
- การตั้งค่า ESlint
- การใช้งาน env
- การเชื่อมต่อ database ด้วย TypeORM
- การใช้งาน TypeORM สำหรับทำ migration
- การทำ CRUD Api
- การใช้งาน CORS
- การใช้งาน Swagger
- การทำ logger
- การทำ unit test ด้วย jest
- การทำ dockerization

# คำสั่ง NestJS-CLI & TypeOrm-CLI & Docker เบื้องต้น

- npx --package @nestjs/cli@latest nest new nest-api
- yarn nest --help
- yarn nest generate module todos
- yarn nest generate controller todos
- yarn nest generate service todos
- yarn typeorm migration:create src/data/migrations/todo
- yarn typeorm migration:run
- yarn typeorm schema:drop
- docker-compose up -d [--build]
- docker-compose images
- docker-compose ps
- docker-compose logs
- docker inspect nest-api
