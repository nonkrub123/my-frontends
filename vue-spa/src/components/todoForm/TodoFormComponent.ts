import { defineComponent, PropType, reactive, watch } from 'vue';
import { Todo } from '@/store/todo/todo.model';
import { TodoSchema } from '@/store/todo/todo.validator';
import Datepicker from '@vuepic/vue-datepicker';

export default defineComponent({
  name: 'TodoFormComponent',
  components: {
    Datepicker,
  },
  props: {
    todo: {
      type: Object as PropType<Todo>
    }
  },
  setup: (props) => {
    const formData = reactive<Todo>({
      name: '',
      date: new Date(),
      isDone: false,
    });
    const errors  = reactive({
      name: false,
      date: false,
    });

    watch(() => props.todo, (todo) => {
      formData.id = todo?.id;
      formData.name = todo?.name ?? '';
      formData.date = todo?.date ?? new Date();
      formData.isDone = todo?.isDone ?? false;
    });

    const validate = () => {
      const validator = TodoSchema.validate(formData, { abortEarly: false }).error;
      const name = validator ? validator.details.some((detail) => detail.path[0] === 'name') : false;
      const date = validator ? validator.details.some((detail) => detail.path[0] === 'date') : false;
  
      errors.name = name;
      errors.date = date;

      return !validator
        ? formData
        : false;
    };

    const clear = () => {
      formData.id = undefined;
      formData.name = '';
      formData.date = new Date();
      formData.isDone = false;
      clearError();
    };

    const clearError = () => {
      errors.name = false;
      errors.date = false;
    };

    return {
      formData,
      errors,
      validate,
      clear,
      clearError,
    };
  },
});
