import { mount } from '@vue/test-utils';
import GoBackComponent from './GoBackComponent.vue';

describe('GoBackComponent', () => {
  it('should creates an instance', () => {
    const wrapper = mount(GoBackComponent);
    expect(wrapper.classes()).toBeDefined();
  });
});
