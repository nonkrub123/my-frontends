import { defineComponent } from 'vue';

export default defineComponent({
  name: 'GoBackComponent',
  emits: [
    'goBack'
  ],
  setup: (_, context) => {
    const goBackClick = () => {
      context.emit('goBack');
    };

    return {
      goBackClick,
    };
  }
});
