import { defineComponent, onMounted, onUnmounted, ref } from 'vue';
import { Modal } from 'bootstrap';

export default defineComponent({
  name: 'ModalComponent',
  emits: [
    'show',
    'shown',
    'hide',
    'hidden',
  ],
  props: {
    backdrop: {
      type: [String as () => 'static', Boolean],
      default: () => true
    },
    size: {
      type: [String as () => 'sm' | 'lg' | 'xl' | 'fullscreen']
    },
    overflow: {
      type: Boolean,
      default: () => false
    }
  },
  setup: (props, context) => {
    const baseModal = ref<HTMLDivElement>();
    const modal = ref<Modal>();

    onMounted(() => {
      const modalElement = baseModal.value as HTMLDivElement;
      modalElement.addEventListener('show.bs.modal', () => context.emit('show'));
      modalElement.addEventListener('shown.bs.modal', () => context.emit('shown'));
      modalElement.addEventListener('hide.bs.modal', () => context.emit('hide'));
      modalElement.addEventListener('hidden.bs.modal', () => context.emit('hidden'));
      modal.value = new Modal(modalElement, { backdrop: props.backdrop });
    });

    onUnmounted(() => {
      modal.value?.dispose();
    });

    const show = () => {
      modal.value?.show();
    };

    const hide = () => {
      modal.value?.hide();
    };

    return {
      baseModal,
      show,
      hide
    };
  }
});
