import { computed, defineComponent, PropType, ref } from 'vue';
import { useTodoStore } from '@/store/todo/todo.store';
import { Todo } from '@/store/todo/todo.model';
import Swal, { SweetAlertOptions } from 'sweetalert2';
import ModalComponent from '@/components/modal/ModalComponent.vue';
import TodoFormComponent from '../todoForm/TodoFormComponent.vue';

export default defineComponent({
  name: 'TodoModalComponent',
  components: {
    ModalComponent,
    TodoFormComponent,
  },
  props: {
    todo: {
      type: Object as PropType<Todo>
    }
  },
  setup: (props) => {
    const todoModal = ref<InstanceType<typeof ModalComponent>>();
    const todoForm = ref<InstanceType<typeof TodoFormComponent>>();
    const store = useTodoStore();
    const status = store.status;   
    const isEditMode = computed(() => !!props.todo);
    const title = computed(() => props.todo ? 'Edit a todo item' : 'New a todo item');
    const icon = computed(() => props.todo ? 'fa-pen' : 'fa-plus');
    const options: SweetAlertOptions = {
      title: 'Delete',
      text: 'Do you want to delete this item ?',
      icon: 'warning',
      confirmButtonColor: '#f86c6b',
      showCancelButton: true,
      focusCancel: true,
    };

    // show modal
    const show = () => {
      todoModal.value?.show();
    };

    // hide modal
    const hide = () => {
      todoModal.value?.hide();
    };

    // submit form
    const submit = async () => {
      const formData = todoForm.value?.validate();

      if (formData) {
        try {
          isEditMode.value
            ? await store.update(formData.id as number, formData.name, formData.date, formData.isDone)
            : await store.create(formData.name, formData.date, formData.isDone);

          hide();
        } catch (ex) {
          console.log(ex);
        }
      }
    };

    // delete
    const remove = async () => {
      const result = await Swal.fire(options);

      if (result.isConfirmed) {
        try {
          const id = props.todo?.id as number;
          await store.remove(id);

          hide();
        } catch (ex) {
          console.log(ex);
        }
      }
    };

    const clearForm = () => {
      todoForm.value?.clear();
    };

    return {
      title,
      icon,
      status,
      isEditMode,
      todoModal,
      todoForm,
      show,
      hide,
      submit,
      remove,
      clearForm,
    };
  },
});
