import { defineComponent, onUnmounted, ref, watchEffect } from 'vue';
import { useTodoStore } from '@/store/todo/todo.store';
import { Todo } from '@/store/todo/todo.model';
import { useDateFormat } from '@/composables/dateFormat';
import Datatables from 'datatables.net';

export default defineComponent({
  name: 'TodoTableComponent',
  emits: [
    'selected'
  ],
  setup: (_, context) => {
    const store = useTodoStore();
    const todos = store.todos;
    const status = store.status;
    const dateFormat = useDateFormat();
    const datatable = ref<HTMLTableElement>();
    const dtInstance = ref<InstanceType<typeof Datatables.Api>>();
    const options = {
      pageLength: 25,
      autoWidth: false,
      columns: [
        { width: '10%' },
        { width: '30%' },
        { width: '30%' },
        { width: '30%' },
      ],
    };
  
    watchEffect(() => {
      const dt = datatable.value;

      if (dt) {
        if ($.fn.dataTable.isDataTable(dt)) {
          dtInstance.value?.destroy();
        }
  
        if (!$.fn.dataTable.isDataTable(dt)) {
          const instance = $(dt).DataTable<HTMLTableElement>(options);
          dtInstance.value = instance;
        }
      }
    });

    onUnmounted(() => {
      dtInstance.value?.destroy();
    });

    const selectedItem = (todo: Todo) => {
      context.emit('selected', todo);
    };

    const thaiDate = (date: Date) =>
      dateFormat.toThaiDate(date, 'long');

    return {
      todos,
      status,
      datatable,
      selectedItem,
      thaiDate,
    };
  }
});
