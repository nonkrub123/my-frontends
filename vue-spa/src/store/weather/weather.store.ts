import { defineStore, storeToRefs } from 'pinia';
import { useToast } from 'vue-toastification';
import type { Status } from '..';
import weatherService from './weather.service';
import type { City } from './weather.model';

// state
interface WeatherState {
  weather: {
    city: City;
    status: Status;
  }
}

// store
const toast = useToast();
const useStore = defineStore('weatherStore', {
  state: (): WeatherState => ({ 
    weather: {
      city: {} as City,
      status: 'idle',
    }
  }),
  getters: {
    city: (state) => state.weather.city,
    status: (state) => state.weather.status,
  },
  actions: {
    async getCity(cityName: string) {
      this.weather.status = 'loading';
      try {
        const response = await weatherService.getCity(cityName);
        this.weather.city = response.data;
        this.weather.status = 'idle';
      } catch (err) {
        toast.error('Oops, we can\'t find that city');
        this.weather.status = 'failed';
      }
    }
  }
});

export const useWeatherStore = () => {
  const store = useStore();

  // getters
  const { city, status } = storeToRefs(store);

  // actions
  const getCity = (cityName: string) => store.getCity(cityName);

  return { city, status, getCity };
};
