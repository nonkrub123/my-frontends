import { AxiosResponse } from 'axios';
import { useAxios } from '@/composables/axios';
import type { Todo } from './todo.model';

const apiUrl = import.meta.env.VITE_API_URL;
const axios = useAxios(apiUrl);

export interface TodoService {
  fetchTodo: () => Promise<AxiosResponse<Todo[]>>;
  createTodo: (name: string, date: Date, isDone: boolean) => Promise<AxiosResponse<Todo>>;
  updateTodo: (id: number, name: string, date: Date, isDone: boolean) => Promise<AxiosResponse<Todo>>;
  deleteTodo: (id: number) => Promise<number>;
}

const fetchTodo = (): Promise<AxiosResponse<Todo[]>> =>
  axios.Get<Todo[]>('/api/todos');

const createTodo = (name: string, date: Date, isDone: boolean): Promise<AxiosResponse<Todo>> => {
  const newTodo = { name, date, isDone };
  return axios.Post<Todo>('/api/todos/', newTodo);
};

const updateTodo = (id: number, name: string, date: Date, isDone: boolean): Promise<AxiosResponse<Todo>> =>  {
  const updateTodo = { name, date, isDone };
  return axios.Put<Todo>(`/api/todos/${id}`, updateTodo);
};

const deleteTodo = async (id: number): Promise<number> => {
  await axios.Delete<void>(`/api/todos/${id}`);
  return id;
};

export default { fetchTodo, createTodo, updateTodo, deleteTodo } as TodoService;
