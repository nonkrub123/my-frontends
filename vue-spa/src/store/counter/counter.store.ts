import { defineStore, storeToRefs } from 'pinia';

// state
interface CounterState {
  counterState: {
    counter: number;
  }
}

// store
const useStore = defineStore('counterStore', {
  state: (): CounterState => ({ 
    counterState: {
      counter: 0
    }
  }),
  getters: {
    counter: (state) => state.counterState.counter
  },
  actions: {
    increment() {
      this.counterState.counter += 1;
    },
    decrement() {
      this.counterState.counter -= 1;
    },
    setAmount(amount: number) {
      this.counterState.counter = amount;
    }
  }
});

export const useCounterStore = () => {
  const store = useStore();

  // getters
  const { counter } = storeToRefs(store);

  // actions
  const increment = () => store.increment();
  const decrement = () => store.decrement();
  const setAmount = (amount: number) => store.setAmount(amount);

  return { counter, increment, decrement, setAmount };
};
