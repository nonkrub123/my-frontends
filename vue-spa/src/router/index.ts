import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router';
import LayoutComponent from '@/components/layout/LayoutComponent.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: LayoutComponent,
    children: [
      {
        path: '',
        name: 'home',
        meta: { title: 'Home' },
        component: () => import(/* webpackChunkName: "home" */ '@/pages/home/HomePage.vue')
      },
      {
        path: 'city',
        name: 'city',
        meta: { title: 'City' },
        component: () => import(/* webpackChunkName: "city" */ '@/pages/city/CityPage.vue')
      },
      {
        path: 'todo',
        name: 'todo',
        meta: { title: 'Todo' },
        component: () => import(/* webpackChunkName: "todo" */ '@/pages/todo/TodoPage.vue')
      },
      {
        path: 'about',
        name: 'about',
        meta: { title: 'About' },
        component: () => import(/* webpackChunkName: "about" */ '@/pages/about/AboutPage.vue')
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'notFound',
    meta: { title: 'Not Found' },
    component: () => import(/* webpackChunkName: "not-found" */ '@/pages/notFound/NotFoundPage.vue')
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
  routes
});

router.beforeEach((to, _, next) => {
  const appName = import.meta.env.VITE_NAME || 'Vue.js';
  document.title = `${to.meta.title} - ${appName}`;
  next();
});

export default router;
