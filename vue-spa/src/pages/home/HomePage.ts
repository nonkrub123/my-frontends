import { defineComponent, onMounted, ref } from 'vue';
import { useCounterStore } from '@/store/counter/counter.store';
import CounterComponent from '@/components/counter/CounterComponent.vue';

export default defineComponent({
  name: 'HomePage',
  components: {
    CounterComponent,
  },
  setup: () => {
    const greeting = ref<HTMLElement>();
    const store = useCounterStore();
    const count = store.counter;

    onMounted(() => {
      if (greeting.value) {
        $(greeting.value).html('Hello Vue.js!!');
      }
    });

    return {
      greeting,
      count,
    };
  }
});
