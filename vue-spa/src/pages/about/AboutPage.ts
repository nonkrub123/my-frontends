import { defineComponent } from 'vue';

export default defineComponent({
  name: 'AboutPage',
  setup: () => {
    const stacks = [
      { group: 'Framework', libraries: ['Vue.js', 'TypeScript', 'Composition API'] },
      { group: 'Tools', libraries: ['Vite', 'ESLint', 'Axios'] },
      { group: 'State Container', libraries: ['Pinia', 'Joi'] },
      { group: 'Testing', libraries: ['Vitest', 'Vue Test Utils'] },
      { group: 'Utilities', libraries: ['jQuery', 'DataTables', 'Datepicker', 'Toastr', 'SweetAlert2'] },
      { group: 'UI', libraries: ['Bootstrap', 'Sass', 'Font Awesome', 'favicon'] },
    ];

    return {
      stacks,
    };
  }
});