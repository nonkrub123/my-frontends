import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface AxiosInstance {
  Get:  <T>(url: string, config?: AxiosRequestConfig) => Promise<AxiosResponse<T>>;
  Post: <T>(url: string, data: unknown, config?: AxiosRequestConfig) => Promise<AxiosResponse<T>>;
  Put: <T>(url: string, data: unknown, config?: AxiosRequestConfig) => Promise<AxiosResponse<T>>;
  Delete: <T>(url: string, config?: AxiosRequestConfig) => Promise<AxiosResponse<T>>;
}

export const useAxios = (uri: string): AxiosInstance => {
  // Initial axios instance
  const axiosInstance = axios.create({ baseURL: uri });

  // Request interceptor for API calls
  axiosInstance.interceptors.request.use(
    (config) => {
      // modify config here
      return config;
    },
    (error) => Promise.reject(error)
  );

  // Response interceptor for API calls
  axiosInstance.interceptors.response.use(
    (response) => {
      handleDates(response.data);
      return response;
    },
    (error) => Promise.reject(error)
  );

  // eslint-disable-next-line
  const handleDates = (obj: any) => {
    if (obj == null || typeof obj !== 'object') {
      return obj;
    }

    const isIsoDateString = (value: unknown): boolean => {
      const isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d*)?/;
      return !!value && typeof value === 'string' && isoDateFormat.test(value);
    };

    Object.entries(obj).forEach(([key, value]) => {
      if (isIsoDateString(value)) {
        obj[key] = new Date(value as string);
      } else if (typeof value === 'object') {
        handleDates(value);
      }
    });
  };

  const Get = <T>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> => {
    return axiosInstance.get<T>(url, config);
  };

  const Post = <T>(url: string, data: unknown, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> => {
    return axiosInstance.post<T>(url, data, config);
  };

  const Put = <T>(url: string, data: unknown, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> =>  {
    return axiosInstance.put<T>(url, data, config);
  };

  const Delete = <T>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> =>  {
    return axiosInstance.delete<T>(url, config);
  };

  return { Get, Post, Put, Delete } as AxiosInstance;
};
