# Learn Vue.js

- การใช้งาน Vue.js 3 Framework
- การใช้งาน TypeScript
- การใช้งาน composition API
- การใช้งาน style scope
- การทำ layout
- การทำ routing
- การทำ axios interceptor
- การตั้งค่า Vite
- การตั้งค่า ESlint
- การใช้งาน Bootstrap Style & Bootstrap Script
- การใช้งาน FontAwesome
- การทำ testing ด้วย vitest
- การทำ favicon สำหรับทุกอุปกรณ์

### Home

- การใช้งาน รูปภาพ assets สำหรับ img
- การใช้งาน SCSS animation
- การใช้งาน Pinia แบบง่าย ๆ
- การใช้งาน jQuery

### City

- การเรียนใช้ค่า .env config
- การใช้งาน component
- การรับส่งค่าผ่าน component
- การใช้งาน external api
- การใช้งาน Pinia
- การใช้งาน รูปภาพ assets สำหรับ css style
- การจัดการ error
- การใช้งาน toast

### Todo

- การใช้เรียกงาน function ของ child component
- การใช้งาน modal
- การใช้งาน form
- การทำ form validate
- การทำ CRUD ด้วย Pinia
- การใช้งาน toast
- การใช้งาน jQuery DataTables.net
- การใช้งาน SweetAlert2

# คำสั่ง create project & Docker

- yarn create vite vue-spa --template vue-ts
- docker-compose up -d [--build]
- docker-compose images
- docker-compose ps
- docker-compose logs
- docker inspect vue-spa
