require('dotenv').config();

module.exports = {
  development: {
    dialect: 'postgres',
    host: process.env.NUXT_DB_HOST,
    port: 5432,
    username: process.env.NUXT_DB_USERNAME,
    password: process.env.NUXT_DB_PASSWORD,
    database: 'nuxt'
  },
  test: {
    dialect: 'postgres',
    host: process.env.NUXT_DB_HOST,
    port: 5432,
    username: process.env.NUXT_DB_USERNAME,
    password: process.env.NUXT_DB_PASSWORD,
    database: 'nuxt'
  },
  production: {
    dialect: 'postgres',
    host: process.env.NUXT_DB_HOST,
    port: 5432,
    username: process.env.NUXT_DB_USERNAME,
    password: process.env.NUXT_DB_PASSWORD,
    database: 'nuxt'
  }
};
