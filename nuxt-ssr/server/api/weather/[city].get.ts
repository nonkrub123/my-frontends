import { sendError } from 'h3';
import { NuxtError } from 'nuxt/dist/app/composables';
import { City } from '@/store/weather/weather.model';

export default defineEventHandler(async (event) => {
  const { city } = event.context.params;
  const cityName = city as string;
  const config = useRuntimeConfig();
  
  try {
    const url = 'https://api.openweathermap.org/data/2.5';
    const response = await $fetch<City>(`${url}/weather?q=${cityName}`, {
      params: {
        units: 'metric',
        appid: config.API_KEY,
      },
    });

    return response;
  } catch (err) {
    const error = createNuxtError(err as NuxtError);
    sendError(event, error);
  }
});

const createNuxtError = (err: NuxtError) => createError({
  statusCode: err.data?.cod,
  statusMessage: err.data?.message,
  data: {},
});
