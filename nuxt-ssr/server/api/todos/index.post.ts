import { sendError } from 'h3';
import { NuxtError } from 'nuxt/dist/app/composables';
import todoRepository from '@/data/models/todo';

export default defineEventHandler(async (event) => {
  const body = await useBody(event);

  try {
    const response = await todoRepository.create({
      name: body.name,
      date: body.date,
      isDone: body.isDone,
    });

    return response;
  } catch (err) {
    sendError(event, err as NuxtError);
  }
});
