import { Sequelize } from 'sequelize';

const config = useRuntimeConfig();
const sequelize = new Sequelize({
  dialect: 'postgres',
  host: config.DB_HOST,
  port: 5432,
  username: config.DB_USERNAME,
  password: config.DB_PASSWORD,
  database: 'next',
  logging: false,
});

export default sequelize;
