'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
     await queryInterface.createTable('todos', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
      },
      date: {
        type: Sequelize.DataTypes.DATE,
        allowNull: false,
      },
      isDone: {
        type: Sequelize.DataTypes.BOOLEAN,
        allowNull: false,
      }
    });
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.dropTable('todos');
  }
};
