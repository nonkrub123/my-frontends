import toast, { PluginOptions, POSITION } from 'vue-toastification';

export default defineNuxtPlugin((nuxtApp) => {
  const options: PluginOptions = { position: POSITION.BOTTOM_RIGHT };
  nuxtApp.vueApp.use(toast, options);
});
