import { defineStore, storeToRefs } from 'pinia';
import { useToast } from 'vue-toastification';
import type { Status } from '..';
import todoService from './todo.service';
import type { Todo } from './todo.model';

// state
interface TodoState {
  todoState: {
    todos: Todo[];
    status: Status;
  }
}

// store
const toast = useToast();
const useStore = defineStore('todoStore', {
  state: (): TodoState => ({
    todoState: {
      todos: [],
      status: 'idle',
    }
  }),
  getters: {
    todos: (state) => state.todoState.todos,
    status: (state) => state.todoState.status,
  },
  actions: {
    load(todos: Todo[]) {
      this.todoState.todos = todos;
    },
    async fetch() {
      this.todoState.status = 'loading';
      try {
        const todos = await todoService.fetchTodo();
        this.todoState.todos = todos;
        this.todoState.status = 'idle';
      } catch (err) {
        this.todoState.status = 'failed';
        toast.error('Oops, we can\'t fetch items');
      }
    },
    async create(name: string, date: Date, isDone: boolean) {
      this.todoState.status = 'loading';
      try {
        const todo = await todoService.createTodo(name, date, isDone);
        this.todoState.todos.push(todo);
        this.todoState.status = 'idle';
        toast.success('Create item successfully.');
      } catch (err) {
        this.todoState.status = 'failed';
        toast.error('Oops, we can\'t find the item');
      }
    },
    async update(id: number, name: string, date: Date, isDone: boolean) {
      this.todoState.status = 'loading';
      try {
        const todo = await todoService.updateTodo(id, name, date, isDone);
        const index = this.todoState.todos.findIndex(m => m.id === todo.id);
        this.todoState.todos[index] = todo;
        this.todoState.status = 'idle';
        toast.success('Update item successfully.');
      } catch (err) {
        this.todoState.status = 'failed';
        toast.error('Oops, we can\'t update the item');
      }
    },
    async remove(id: number) {
      this.todoState.status = 'loading';
      try {
        const deleted = await todoService.deleteTodo(id);
        const index = this.todoState.todos.findIndex(m => m.id === id);
        this.todoState.todos.splice(index, 1);
        this.todoState.status = 'idle';
        toast.success('Delete item successfully.');
      } catch (err) {
        this.todoState.status = 'failed';
        toast.error('Oops, we can\'t delete the item');
      }
    },
  }
});

export const useTodoStore = () => {
  const store = useStore();

  // getters
  const { todos, status } = storeToRefs(store);

  // actions
  const load = (todos: Todo[]) => store.load(todos);
  const fetch = () => store.fetch();
  const create = (name: string, date: Date, isDone: boolean) => store.create(name, date, isDone);
  const update = (id: number, name: string, date: Date, isDone: boolean) => store.update(id, name, date, isDone);
  const remove = (id: number) => store.remove(id);

  return { todos, status, load, fetch, create, update, remove };
};
