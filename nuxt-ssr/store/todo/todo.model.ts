export interface Todo {
  id?: number;
  name: string;
  date: Date;
  isDone: boolean;
}
