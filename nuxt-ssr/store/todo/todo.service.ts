import { useFetchInstance } from '@/composables/fetch';
import type { Todo } from './todo.model';

const fetch = useFetchInstance('/');

export interface TodoService {
  fetchTodo: () => Promise<Todo[]>;
  createTodo: (name: string, date: Date, isDone: boolean) => Promise<Todo>;
  updateTodo: (id: number, name: string, date: Date, isDone: boolean) => Promise<Todo>;
  deleteTodo: (id: number) => Promise<void>;
}

const fetchTodo = async (): Promise<Todo[]> =>
  fetch.Get<Todo[]>('/api/todos');

const createTodo = (name: string, date: Date, isDone: boolean): Promise<Todo> => 
  fetch.Post<Todo>('/api/todos', {
    name: name,
    date: date,
    isDone: isDone,
  });

const updateTodo = (id: number, name: string, date: Date, isDone: boolean): Promise<Todo> => 
  fetch.Put<Todo>(`/api/todos/${id}`, {
    name: name,
    date: date,
    isDone: isDone,
  });

const deleteTodo = (id: number): Promise<void> =>
  fetch.Delete<void>(`/api/todos/${id}`);

export default { fetchTodo, createTodo, updateTodo, deleteTodo } as TodoService;
