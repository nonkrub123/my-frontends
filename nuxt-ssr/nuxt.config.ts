// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  typescript: {
    shim: false,
    strict: true,
  },
  runtimeConfig: {
    API_KEY: process.env.NUXT_API_KEY,
    DB_HOST: process.env.NUXT_DB_HOST,
    DB_USERNAME: process.env.NUXT_DB_USERNAME,
    DB_PASSWORD: process.env.NUXT_DB_PASSWORD,
  },
  css: [
    '@/assets/styles/global.scss'
  ],
  buildModules: ['@pinia/nuxt']
});
