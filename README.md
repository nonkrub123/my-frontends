### Environment
- node v16.x
- yarn
- docker

### OpenWeather API Key
- goto https://openweathermap.org/
- sign in
- your account -> my api key -> generate
- copy api-key
- first time, please check your email to verify account

### PostgreSQL (Database)
- cd postgres-db
- docker volume create postgresql_data
- docker-compose up -d
- for next time, you can run "docker-compose start" to start database

### NestJS (API)
- cd nest-app
- cp .env.example .env
- open .env
- add port number into .env (recommand port: 8000)
- yarn install
- yarn typeorm migration:run
- yarn start:dev
- goto http://localhost:8000 to see api documents
- finally, SPA projects can use the api

### Angular (SPA)
- cd angular-app
- cp .env.example .env
- open .env
- paste OpenWeather API Key into .env
- add API_URL (http://localhost:8000) into .env
- yarn install
- yarn serve
- goto http://localhost:4200
- you must run NestJS project for provide the api

### React (SPA)
- cd react-app
- cp .env.example .env
- open .env
- paste OpenWeather API Key into .env
- add API_URL (http://localhost:8000) into .env
- yarn install
- yarn dev
- goto http://localhost:3000
- you must run NestJS project for provide the api

### Vue.js (SPA)
- cd react-app
- cp .env.example .env
- open .env
- paste OpenWeather API Key into .env
- add API_URL (http://localhost:8000) into .env
- yarn install
- yarn dev
- goto http://localhost:3000
- you must run NestJS project for provide the api

### Next.js (SSR)
- cd next-app
- cp .env.example .env
- open .env
- paste api-key into .env
- yarn install
- yarn sequelize db:migrate
- yarn dev
- goto http://localhost:3000

### Nuxt (SSR)
- cd nuxt-app
- cp .env.example .env
- open .env
- paste api-key into .env
- yarn install
- yarn sequelize db:migrate
- yarn dev
- goto http://localhost:3000
