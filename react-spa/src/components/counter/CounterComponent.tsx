import React from 'react';
import { useAppSelector, useAppDispatch } from '@/hooks/redux';
import { CounterAction, CounterSelector } from '@/store/counter/counterSlice';
import styles from './CounterComponent.module.scss';

const CounterComponent = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const count = useAppSelector(CounterSelector.counter);
  
  return (
    <div>
      <button
        className="btn btn-primary"
        aria-label="Decrement value"
        onClick={() => dispatch(CounterAction.decrement())}
      >
        <span className="fas fa-minus-circle"></span>
      </button>
      <div className={styles.counter}>{count}</div>
      <button
        className="btn btn-primary"
        aria-label="Increment value"
        onClick={() => dispatch(CounterAction.increment())}
      >
        <span className="fas fa-plus-circle"></span>
      </button>
    </div>
  );
};

export default CounterComponent;
