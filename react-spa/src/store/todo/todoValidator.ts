import Joi from 'joi';

export const TodoSchema = Joi.object({
  name: Joi.string().required(),
  date: Joi.date().required(),
}).options({ allowUnknown: true });
