import React from 'react';
import type { RouteObject } from 'react-router-dom';
import Page from './Page';
import LayoutComponent from '@/components/layout/LayoutComponent';

const HomePage = React.lazy(() => import('@/pages/home/HomePage'));
const CityPage = React.lazy(() => import('@/pages/city/CityPage'));
const TodoPage = React.lazy(() => import('@/pages/todo/TodoPage'));
const AboutPage = React.lazy(() => import('@/pages/about/AboutPage'));
const NotFoundPage = React.lazy(() => import('@/pages/notFound/NotFoundPage'));

const routes: RouteObject[] = [
  {
    path: '/',
    element: <LayoutComponent />,
    children: [
      { 
        index: true,
        element: <Page title="Home"><HomePage /></Page>,
      },
      {
        path: 'city',
        element: <Page title="City"><CityPage /></Page>,
      },
      {
        path: 'todo',
        element: <Page title="Todo"><TodoPage /></Page>,
      },
      {
        path: 'about',
        element: <Page title="About"><AboutPage /></Page>,
      },
    ],
  },
  { 
    path: '*',
    element: <Page title="Not Found"><NotFoundPage /></Page>,
  },
];

export default routes;
