import React from 'react';

type Props = {
  title: string,
  children?: JSX.Element,
};

const Page = (props: Props): JSX.Element => {
  const appName = import.meta.env.VITE_NAME || 'React';
  
  React.useEffect(() => {
    document.title = `${props.title} - ${appName}`;
  }, [props.title]);
  
  return <>{props.children}</>;
};

export default Page;
