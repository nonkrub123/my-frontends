import React from 'react';
import { useAppSelector, useAppDispatch } from '@/hooks/redux';
import { WeatherAction, WeatherSelector } from '@/store/weather/weatherSlice';
import GoBackComponent from '@/components/goBack/GoBackComponent';
import WeatherComponent from '@/components/weather/WeatherComponent';
import styles from './CityPage.module.scss';
import fall from '@/assets/images/fall.jpg';
import spring from '@/assets/images/spring.jpg';
import summer from '@/assets/images/summer.jpg';
import winter from '@/assets/images/winter.jpg';

const WeatherPage = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const city = useAppSelector(WeatherSelector.city);
  const status = useAppSelector(WeatherSelector.status);
  const [search, setSearch] = React.useState('bangkok');
  const [cityName, setCityName] = React.useState(search);
  const [background, setBackground] = React.useState('');
  
  React.useEffect(() => {
    dispatch(WeatherAction.getCity(search));
  }, [search]);

  React.useEffect(() => {
    if (status === 'idle') {
      setCityName(search);
      setBackground(getBackground(city.main?.temp ?? 0));
    }
  }, [status]);

  const getBackground = (temperature: number) => {
    const bg = (temperature > 0)
      ? (temperature > 10)
        ? (temperature > 20)
          ? summer
          : spring
        : fall
      : winter;
    return `url(${bg})`;
  };

  return (
    <main className={styles.main}>
      <div
        className={`${styles.city} d-flex justify-content-center align-items-center`}
        style={{backgroundImage: background}}
      >
        <div className="container h-100">
          <div className="row h-100">
            <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
              <div className="d-table-cell align-middle">
                {status !== 'failed'
                  ? <WeatherComponent city={city} onSearch={(name) => setSearch(name)} />
                  : <GoBackComponent onGoBack={() => setSearch(cityName)} />
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default WeatherPage;
