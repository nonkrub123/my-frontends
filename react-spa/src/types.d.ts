// jquery
declare module 'jquery' { export = $; }
declare const jQuery: JQueryStatic;
declare const $: JQueryStatic;

// datatables
declare module 'datatables.net-bs5';
