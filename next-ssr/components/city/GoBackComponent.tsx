import React from 'react';

type Props = {
  onGoBack: React.MouseEventHandler<HTMLButtonElement>
};

const GoBackComponent = (props: Props): JSX.Element => {
  return (<>
    <div className="panel text-center">
      <h1>Oops, we can&apos;t find that city</h1>
      <button
        type="button"
        className="btn btn-primary mt-5"
        onClick={props.onGoBack}
      >
        <span className="fa-solid fa-rotate-left me-2"></span>Go Back
      </button>
    </div>
    <style jsx>{`
      .panel {
        background-color: rgba($color: #000000, $alpha: 0.25);
        padding: 2rem;
        border-radius: 20px;
      }   
    `}</style>
  </>);
};

export default GoBackComponent;
