import { render, screen, within } from '@testing-library/react';
import GoBackComponent from './GoBackComponent';

describe('GoBackComponent', () => {
  it('should render the component', () => {
    render(<GoBackComponent onGoBack={() => ''} />);

    expect(within(screen.getByText(/Oops, we can't find that city/i))).toBeDefined();
  });
});
