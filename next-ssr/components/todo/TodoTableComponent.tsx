import React from 'react';
import { useAppSelector } from '@/hooks/redux';
import { TodoSelector } from '@/store/todo/todoSlice';
import { Todo } from '@/store/todo/todoModel';
import { useDateFormat } from '@/hooks/dateFormat';
import $ from 'jquery';
import Datatables from 'datatables.net';
import dataTables from 'datatables.net-bs5';

type Props = {
  onSelected: React.Dispatch<Todo>;
}

const TodoTableComponent = (props: Props): JSX.Element => {
  const todos = useAppSelector(TodoSelector.todos);
  const status = useAppSelector(TodoSelector.status);
  const dateFormat = useDateFormat();
  const datatable = React.useRef<HTMLTableElement>(null);
  const [dtInstance, setDtInstance] = React.useState<InstanceType<typeof Datatables.Api>>();
  const options = {
    pageLength: 25,
    autoWidth: false,
    columns: [
      { width: '10%' },
      { width: '30%' },
      { width: '30%' },
      { width: '30%' },
    ],
  };

  React.useEffect(() => {
    new dataTables();
    
    return () => {
      dtInstance?.destroy();
    };
  }, []);

  React.useEffect(() => {
    const dt = datatable.current;

    if (dt) {
      if ($.fn.dataTable.isDataTable(dt)) {
        dtInstance?.destroy();
      }

      if (!$.fn.dataTable.isDataTable(dt)) {
        const instance = $(dt).DataTable<HTMLTableElement>(options);
        setDtInstance(instance);
      }
    }
  }, [todos]);

  const thaiDate = (date: Date) =>
    dateFormat.toThaiDate(date, 'long');

  return (<>
    {status !== 'loading' &&
      <div className="table-responsive mt-3">
        <table ref={datatable} className="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Todo</th>
              <th scope="col">Date</th>
              <th scope="col">Done</th>
            </tr>
          </thead>
          <tbody>
            {todos.map((todo, index) => 
              <tr
                key={todo.id}
                className="hover"
                onClick={() => props.onSelected(todo)}
              >
                <td>{index + 1}.</td>
                <td className="text-primary">
                  <span className="far fa-note-sticky me-2"></span>{todo.name}
                </td>
                <td>{thaiDate(todo.date)}</td>
                <td className={todo.isDone ? 'text-success' : 'text-danger'}>
                  <span className={`far ${todo.isDone ? 'fa-circle-check' : 'fa-circle-xmark'}`}></span>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    }
    <style jsx>{`
      .hover {
        cursor: pointer;
      }
    `}</style>
  </>);
};

export default TodoTableComponent;