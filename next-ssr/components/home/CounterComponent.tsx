import { useAppSelector, useAppDispatch } from '@/hooks/redux';
import { CounterAction, CounterSelector } from '@/store/counter/counterSlice';

const CounterComponent = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const count = useAppSelector(CounterSelector.counter);
  
  return (<>
    <div>
      <button
        className="btn btn-primary"
        aria-label="Decrement value"
        onClick={() => dispatch(CounterAction.decrement())}
      >
        <span className="fas fa-minus-circle"></span>
      </button>
      <div className="counter">{count}</div>
      <button
        className="btn btn-primary"
        aria-label="Increment value"
        onClick={() => dispatch(CounterAction.increment())}
      >
        <span className="fas fa-plus-circle"></span>
      </button>
    </div>
    <style jsx>{`
      .counter {
        display: inline-block;
        width: 80px;
        text-align: center;
      }    
    `}</style>
  </>);
};

export default CounterComponent;
