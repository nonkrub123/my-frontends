# Learn Next.js

- การใช้งาน Next 12 Framework
- การใช้งาน TypeScript
- การใช้งาน functional component
- การใช้งาน JSX
- การใช้งาน style JSX
- การทำ layout
- การทำ file-system based routing
- การทำ internal api
- การทำ axios interceptor
- การเชื่อมต่อ database ด้วย Sequelize
- การใช้งาน Sequelize-CLI สำหรับทำ migration
- การใช้งาน CORS
- การตั้งค่า Vite
- การตั้งค่า ESlint
- การใช้งาน Bootstrap Style & Bootstrap Script
- การใช้งาน FontAwesome
- การทำ testing ด้วย vitest
- การทำ favicon สำหรับทุกอุปกรณ์

### Home

- การใช้งาน getInitialProps
- การใช้งาน รูปภาพ assets สำหรับ img
- การใช้งาน SCSS animation
- การใช้งาน React Toolkit แบบง่าย ๆ
- การใช้งาน jQuery

### City

- การเรียนใช้ค่า .env config
- การใช้งาน cookies
- การใช้งาน component
- การรับส่งค่าผ่าน component
- การใช้ internal api เรียก external api
- การใช้งาน internal api
- การใช้งาน React Toolkit
- การใช้งาน รูปภาพ assets สำหรับ css style
- การจัดการ error
- การใช้งาน toast

### Todo

- การใช้เรียกงาน function ของ child component
- การใช้งาน modal
- การใช้งาน form
- การทำ form validate
- การใช้งาน internal api
- การทำ CRUD ด้วย React Toolkit
- การใช้งาน toast
- การใช้งาน jQuery DataTables.net
- การใช้งาน SweetAlert2

# คำสั่ง Sequelize-CLI & Docker เบื้องต้น

- npx --package create-next-app@latest next-ssr --typescript
- yarn sequelize migration:generate --name todo
- yarn sequelize db:create
- yarn sequelize db:drop
- yarn sequelize db:migrate
- yarn sequelize db:migrate:undo
- yarn sequelize db:migrate:undo:all
- docker-compose up -d [--build]
- docker-compose images
- docker-compose ps
- docker-compose logs
- docker inspect next-ssr
