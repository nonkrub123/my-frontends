module.exports = {
  apps : [{
    name: 'NextSSR',
    script: './node_modules/.bin/next',
    args: 'start',
    exec_mode: 'cluster',
    instances: 'max',
    autorestart: true,
    env: {
      PORT: 3000,
      NODE_ENV: 'production'
    }
  }]
};
