import { AxiosResponse } from 'axios';
import { useAxios } from '@/hooks/axios';
import type { City } from './weatherModel';

const axios = useAxios('/');

export interface WeatherService {
  getCity: (cityName: string) => Promise<AxiosResponse<City>>;
}

const getCity = (cityName: string): Promise<AxiosResponse<City>> =>
  axios.Get<City>(`/api/weather/${cityName}`);

export default { getCity } as WeatherService;
