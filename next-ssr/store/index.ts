import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer, { COUNTER_STORE_NAMESPACE } from './counter/counterSlice';
import weatherReducer, { WEATHER_STORE_NAMESPACE } from './weather/weatherSlice';
import todoReducer, { TODO_STORE_NAMESPACE } from './todo/todoSlice';

export const store = configureStore({
  reducer: {
    [COUNTER_STORE_NAMESPACE]: counterReducer,
    [WEATHER_STORE_NAMESPACE]: weatherReducer,
    [TODO_STORE_NAMESPACE]: todoReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
export type RootState = ReturnType<typeof store.getState>;
export type Status = 'idle' | 'loading' | 'failed';
