import { AxiosResponse } from 'axios';
import { useAxios } from '@/hooks/axios';
import type { Todo } from './todoModel';

const axios = useAxios('/');

export interface TodoService {
  fetchTodo: () => Promise<AxiosResponse<Todo[]>>;
  createTodo: (name: string, date: Date, isDone: boolean) => Promise<AxiosResponse<Todo>>;
  updateTodo: (id: number, name: string, date: Date, isDone: boolean) => Promise<AxiosResponse<Todo>>;
  deleteTodo: (id: number) => Promise<AxiosResponse<void>>;
}

const fetchTodo = (): Promise<AxiosResponse<Todo[]>> =>
  axios.Get<Todo[]>('/api/todos');

const createTodo = (name: string, date: Date, isDone: boolean): Promise<AxiosResponse<Todo>> => 
  axios.Post<Todo>('/api/todos', {
    name: name,
    date: date,
    isDone: isDone,
  });

const updateTodo = (id: number, name: string, date: Date, isDone: boolean): Promise<AxiosResponse<Todo>> => 
  axios.Put<Todo>(`/api/todos/${id}`, {
    name: name,
    date: date,
    isDone: isDone,
  });

const deleteTodo = (id: number): Promise<AxiosResponse<void>> =>
  axios.Delete<void>(`/api/todos/${id}`);

export default { fetchTodo, createTodo, updateTodo, deleteTodo } as TodoService;
