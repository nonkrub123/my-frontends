import { ActionReducerMapBuilder, createAsyncThunk, createSlice, isFulfilled, isPending, isRejected, PayloadAction } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import type { RootState, Status } from '../';
import todoService from './todoService';
import { Todo } from './todoModel';

// namespace
export const TODO_STORE_NAMESPACE = 'todoStore';

// state
interface TodoState {
  todos: Todo[];
  status: Status;
}

const initialState: TodoState = {
  todos: [],
  status: 'idle'
};

// reducer
const reducers = {
  load: (state: TodoState, action: PayloadAction<Todo[]>) => {
    state.todos = action.payload;
  },
};

// extra reducer
const fetch = createAsyncThunk(`${TODO_STORE_NAMESPACE}/fetch`, async (_, { rejectWithValue }) => {
  try {
    const respone = await todoService.fetchTodo();
    return respone.data;
  } catch (err) {
    toast.error('Oops, we can\'t fetch items.');
    return rejectWithValue(err);
  }
});

const create = createAsyncThunk(`${TODO_STORE_NAMESPACE}/create`, async (request: Todo, { rejectWithValue }) => {
  try {
    const respone = await todoService.createTodo(request.name, request.date, request.isDone);
    toast.success('Create item successfully.');
    return respone.data;
  } catch (err) {
    toast.error('Oops, we can\'t create the items');
    return rejectWithValue(err);
  }
});

const update = createAsyncThunk(`${TODO_STORE_NAMESPACE}/update`, async (request: Todo, { rejectWithValue }) => {
  try {
    const respone = await todoService.updateTodo(request.id as number, request.name, request.date, request.isDone);
    toast.success('Update item successfully.');
    return respone.data;
  } catch (err) {
    toast.error('Oops, we can\'t update the item');
    return rejectWithValue(err);
  }
});

const remove = createAsyncThunk(`${TODO_STORE_NAMESPACE}/remove`, async (id: number, { rejectWithValue }) => {
  try {
    await todoService.deleteTodo(id);
    toast.success('Delete item successfully.');
    return id;
  } catch (err) {
    toast.error('Oops, we can\'t delete the item');
    return rejectWithValue(err);
  }
});

// mapper
const actionReducerMap = (builder: ActionReducerMapBuilder<TodoState>) => {
  builder
    .addCase(fetch.fulfilled, (state, action) => {
      state.todos = action.payload;
    })
    .addCase(create.fulfilled, (state, action) => {
      state.todos.push(action.payload);
    })
    .addCase(update.fulfilled, (state, action) => {
      const index = state.todos.findIndex(m => m.id === action.payload.id);
      state.todos[index] = action.payload;
    })
    .addCase(remove.fulfilled, (state, action) => {
      const index = state.todos.findIndex(m => m.id === action.payload);
      state.todos.splice(index, 1);
    })
    .addMatcher(isFulfilled(fetch, create, update, remove), (state) => {
      state.status = 'idle';
    })
    .addMatcher(isPending(fetch, create, update, remove), (state) => {
      state.status = 'loading';
    })
    .addMatcher(isRejected(fetch, create, update, remove), (state) => {
      state.status = 'failed';
    });
};

// slice
const TodoSlice = createSlice({
  name: TODO_STORE_NAMESPACE,
  initialState,
  reducers: reducers,
  extraReducers: (builder) => actionReducerMap(builder)
});

// selector
export const TodoSelector = {
  todos: (state: RootState) => state.todoStore.todos,
  todo: (id: number) => (state: RootState) => state.todoStore.todos.find(m => m.id === id),
  status: (state: RootState) => state.todoStore.status,
};

// action
export const TodoAction = {
  load: TodoSlice.actions.load,
  fecth: () => fetch(),
  create: (name: string, date: Date, isDone: boolean) => create({ name, date, isDone }),
  update: (id: number, name: string, date: Date, isDone: boolean) => update({ id, name, date, isDone }),
  remove: (id: number) => remove(id),
};

export default TodoSlice.reducer;
