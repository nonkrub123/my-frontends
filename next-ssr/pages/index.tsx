import type { NextPage } from 'next';
import Head from 'next/head';
import React from 'react';
import $ from 'jquery';
import LayoutComponent from '@/components/layout/LayoutComponent';
import SlidingComponent from '@/components/home/SlidingComponent';
import CounterComponent from '@/components/home//CounterComponent';
import { useAppSelector } from '@/hooks/redux';
import { CounterSelector } from '@/store/counter/counterSlice';
import logo from '@/assets/images/logo.svg';

const HomePage: NextPage = () => {
  const title = 'Home';
  const greeting = React.useRef<HTMLElement>(null);
  const count = useAppSelector(CounterSelector.counter);

  React.useEffect(() => {
    if (greeting.current) {
      $(greeting.current).html('Hello Next.js!!');
    }
  }, []);

  return (<>
    <Head>
      <title>{`${title} - Next.js`}</title>
    </Head>
    <LayoutComponent>
      <main>
        <div className="background">
          <SlidingComponent />
        </div>

        <div className="next d-flex justify-content-center align-items-center">
          <div className="text-center">
            <h4>
              <span className="fab fa-react h4 me-2"></span>
              <span ref={greeting} className="h4"></span>
            </h4>
            <div className="next-logo my-5"></div>
            <CounterComponent />
          </div>
        </div>

        <footer className="footer fixed-bottom py-3 bg-dark">
          <div className="container d-flex flex-row-reverse">
            <div className="text-light">Counter: {count}</div>
          </div>
        </footer>
      </main>
    </LayoutComponent>
    <style jsx>{`
      .background {
        overflow: hidden;
      }

      .next {
        position: absolute;
        left: 0;
        top: 60px;
        width: 100vw;
        min-height: calc(100vh - 60px);
        text-align: center;
        color: rgba($color: #000000, $alpha: 0.8);
      }

      .next-logo {
        width: 45vmin;
        height: 27vmin;
        pointer-events: none;
        background-image: url(${logo.src});
        background-size: cover;
      }
    `}</style>
  </>);
};

export default HomePage;
