import type { NextPage } from 'next';
import Head from 'next/head';
import LayoutComponent from '@/components/layout/LayoutComponent';

const AboutPage: NextPage = () => {
  const title = 'About';
  const stacks = [
    { group: 'Framework', libraries: ['Next.js', 'React', 'TypeScript', 'JSX', 'Style JSX'] },
    { group: 'Tools', libraries: ['Vite', 'ESLint', 'Cookie', 'Axios', 'CORS'] },
    { group: 'Database', libraries: ['Sequelize', 'Sqlite'] },
    { group: 'State Container', libraries: ['Redux', 'Redux Toolkit', 'Joi'] },
    { group: 'Testing', libraries: ['Vitest', 'React Testing Library'] },
    { group: 'Utilities', libraries: ['React-Bootstrap', 'jQuery', 'DataTables', 'Toastr', 'SweetAlert2'] },
    { group: 'UI', libraries: ['Bootstrap', 'Sass', 'Font Awesome', 'favicon'] },
  ];

  return (<>
    <Head>
      <title>{`${title} - Next.js`}</title>
    </Head>
    <LayoutComponent>
      <main>
        <div className="container my-4">
          <div className="card">
            <div className="card-body">
              <table className="table table-hover">
                <thead className="table-light">
                  <tr>
                    <th style={{width: '10%'}}>#</th>
                    <th style={{width: '30%'}}>Stack</th>
                    <th style={{width: '60%'}}>library</th>
                  </tr>
                </thead>
                <tbody>
                  {stacks.map((stack, i) =>
                    stack.libraries.map((lib, j) => (j === 0)
                      ? <tr key={lib}>
                          <td>{i + 1}.</td>
                          <td className="text-primary">{stack.group}</td>
                          <td>{lib}</td>
                        </tr>
                      : <tr key={lib}>
                          <td colSpan={2}>&nbsp;</td>
                          <td>{lib}</td>
                        </tr>
                    )
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </main>
    </LayoutComponent>
    <style jsx>{`
      tbody {
        tr {
          &:last-child {
            > th,td {
              border-bottom: 0;
            }
          }
        }
      }        
    `}</style>
  </>);
};

export default AboutPage;
