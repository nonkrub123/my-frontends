import type { GetServerSideProps, NextPage } from 'next';
import React from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import absoluteUrl from 'next-absolute-url';
import { getCookie, setCookie } from 'cookies-next';
import { useAppSelector, useAppDispatch } from '@/hooks/redux';
import { WeatherAction, WeatherSelector } from '@/store/weather/weatherSlice';
import type { City } from '@/store/weather/weatherModel';
import LayoutComponent from '@/components/layout/LayoutComponent';
import fall from '@/assets/images/fall.jpg';
import spring from '@/assets/images/spring.jpg';
import summer from '@/assets/images/summer.jpg';
import winter from '@/assets/images/winter.jpg';

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const cookie = getCookie('city', { req, res });
  if (!cookie) setCookie('city', 'bangkok', { req, res, sameSite: 'strict' });

  const { origin } = absoluteUrl(req);
  const response = await fetch(`${origin}/api/weather/${cookie}`);
  const data: City = await response.json();

  return { 
    props: { data }
  };
};

const CityPage: NextPage<{ data: City }> = ({ data }) => {
  const title = 'City';
  const WeatherComponent = dynamic(() => import('@/components/city/WeatherComponent'));
  const GoBackComponent = dynamic(() => import('@/components/city/GoBackComponent'));
  const cookie = getCookie('city');

  const dispatch = useAppDispatch();
  const city = useAppSelector(WeatherSelector.city);
  const status = useAppSelector(WeatherSelector.status);
  const [search, setSearch] = React.useState<string>(cookie as string);
  const [background, setBackground] = React.useState('');

  React.useEffect(() => {
    dispatch(WeatherAction.load(data));
  }, [data]);

  React.useEffect(() => {
    dispatch(WeatherAction.getCity(search));
  }, [search]);

  React.useEffect(() => {
    if (status === 'idle') {
      setCookie('city', search, { sameSite: 'strict' });
      setBackground(getBackground(city.main?.temp ?? 0));
    }
  }, [status]);

  const getBackground = (temperature: number) => {
    const bg = (temperature > 0)
      ? (temperature > 10)
        ? (temperature > 20)
          ? summer
          : spring
        : fall
      : winter;
    return `url(${bg.src})`;
  };

  return (<>
    <Head>
      <title>{`${title} - Next.js`}</title>
    </Head>
    <LayoutComponent>
      <main className="main">
        <div
          className="city d-flex justify-content-center align-items-center"
          style={{backgroundImage: background}}
        >
          <div className="container h-100">
            <div className="row h-100">
              <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                <div className="d-table-cell align-middle">
                  {status !== 'failed'
                    ? <WeatherComponent city={city} onSearch={(name) => setSearch(name)} />
                    : <GoBackComponent onGoBack={() => setSearch(cookie as string)} />
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </LayoutComponent>
    <style jsx>{`
      .main {
        color: white;
      }   
      
      .city {
        position: relative;
        height: calc(100vh - 60px);
        width: 100vw;
        background: lightgray;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
      }  
    `}</style>
  </>);
};

export default CityPage;
