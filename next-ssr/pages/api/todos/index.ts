// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { useCors } from '@/hooks/cors';
import todoRepository from '@/data/models/todo';
import type { Todo } from '@/store/todo/todoModel';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  // cors
  await useCors(req, res);

  // request
  const body: Todo = req.body;

  // Rest of the API logic
  const fetch = async () => {
    try {
      const response = await todoRepository.findAll();
  
      return res.status(200).json(response);
    } catch (err) {
      return res.status(500).json(err);
    }
  };

  const create = async () => {
    try {
      const response = await todoRepository.create({
        name: body.name,
        date: body.date,
        isDone: body.isDone,  
      });
  
      return res.status(201).json(response);
    } catch (err) {
      return res.status(500).json(err);
    }
  };

  switch (req.method) {
    case 'GET':
      return fetch();
    case 'POST':
      return create();
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`);
  }
};


export const config = {
  api: {
    bodyParser: true,
  },
};

export default handler;
