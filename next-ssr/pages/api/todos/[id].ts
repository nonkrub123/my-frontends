// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { useCors } from '@/hooks/cors';
import todoRepository from '@/data/models/todo';
import type { Todo } from '@/store/todo/todoModel';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  // cors
  await useCors(req, res);

  // request
  const { id } = req.query;
  const todoId = parseInt(id as string, 10);
  const body: Todo = req.body;

  // Rest of the API logic
  const update = async () => {
    try {
      await todoRepository.update({
        name: body.name,
        date: body.date,
        isDone: body.isDone,
      },
      {
        where: { id: todoId }
      });
  
      const response = await todoRepository.findByPk(todoId);
  
      return res.status(200).json(response);
    } catch (err) {
      return res.status(500).json(err);
    }
  };
  
  const remove = async () => {
    try {
      await todoRepository.destroy({ where: { id: todoId } });
  
      return res.status(204).end();
    } catch (err) {
      return res.status(500).json(err);
    }
  };

  switch (req.method) {
    case 'PUT':
      return update();
    case 'DELETE':
      return remove();
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`);
  }
};

export const config = {
  api: {
    bodyParser: true,
  },
};

export default handler;
