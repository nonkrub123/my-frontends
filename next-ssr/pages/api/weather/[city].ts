// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { useCors } from '@/hooks/cors';
import type { City } from '@/store/weather/weatherModel';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  // cors
  await useCors(req, res);

  // request
  const { city } = req.query;
  const cityName = city as string;

  // Rest of the API logic
  switch (req.method) {
    case 'GET':
      return getCity(res, cityName);
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`);
  }
};

const getCity = async (res: NextApiResponse, cityName: string) => {
  try {
    const apiKey = process.env.API_KEY;
    const url = 'https://api.openweathermap.org/data/2.5';
    const response = await fetch(`${url}/weather?q=${cityName}&units=metric&appid=${apiKey}`);
    const data: City = await response.json();

    return res.status(data.cod).json(data);
  } catch (err) {
    return res.status(500).json(err);
  }
};

export default handler;
