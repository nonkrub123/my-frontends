/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['openweathermap.org'],
    formats: ['image/avif', 'image/webp'],
  },
};

module.exports = nextConfig;
