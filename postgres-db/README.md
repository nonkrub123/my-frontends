# Learn PostgreSQL

- การใช้งาน PostgreSQL ด้วย command line
- การใช้งานคำสั่ง docker-compose

# คำสั่ง Docker & PostgreSQL เบื้องต้น

- docker volume create postgresql_data
- docker-compose up -d
- docker-compose start
- docker-compose stop
- docker exec -it postgresql psql -U admin -d postgres
- CREATE DATABASE "nest";
- CREATE DATABASE "next";
- CREATE DATABASE "nuxt";
- GRANT CONNECT ON DATABASE "nest" TO "admin";
- GRANT CONNECT ON DATABASE "next" TO "admin";
- GRANT CONNECT ON DATABASE "nuxt" TO "admin";
