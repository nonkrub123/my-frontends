import { createReducer, on } from '@ngrx/store';
import * as counterAction from './counter.actions';

// feature name
export const COUNTER_FEATURE_NAME = 'counterStore';

// state
export interface CounterState {
  counter: number;
}

const initialState: CounterState = {
  counter: 0,
};

// reducer
export const counterReducer = createReducer(
  initialState,
  on(counterAction.increment, (state): CounterState => ({
    ...state,
    counter: state.counter + 1,
  })),
  on(counterAction.decrement, (state): CounterState => ({
    ...state,
    counter: state.counter - 1,
  })),
  on(counterAction.setAmount, (state, { amount }): CounterState => ({
    ...state,
    counter: amount,
  })),
);
