import { createAction, props } from '@ngrx/store';
import { Todo } from './todo.model';

enum TodoTypes {
  FETCH = '[Todo] Invoke Todo Fetch',
  FETCH_SUCCESS = '[Todo] Invoke Todo Fetch Success',
  FETCH_ERROR = '[Todo] Invoke Todo Fetch Error',
  CREATE = '[Todo] Invoke Todo Create',
  CREATE_SUCCESS = '[Todo] Invoke Todo Create Success',
  CREATE_ERROR = '[Todo] Invoke Todo Create Error',
  UPDATE = '[Todo] Invoke Todo Update',
  UPDATE_SUCCESS = '[Todo] Invoke Todo Update Success',
  UPDATE_ERROR = '[Todo] Invoke Todo Update Error',
  DELETE = '[Todo] Invoke Todo Delete',
  DELETE_SUCCESS = '[Todo] Invoke Todo Delete Success',
  DELETE_ERROR = '[Todo] Invoke Todo Delete Error',
  DISPLAY_TOAST = '[Todo] Toast Notification',
}

export const fetchTodo = createAction(TodoTypes.FETCH);
export const fetchTodoSuccess = createAction(TodoTypes.FETCH_SUCCESS, props<{ todos: Todo[] }>());
export const fetchTodoError = createAction(TodoTypes.FETCH_ERROR, props<{ error: Error }>());
export const createTodo = createAction(TodoTypes.CREATE, props<{ name: string, date: Date, isDone: boolean }>());
export const createTodoSuccess = createAction(TodoTypes.CREATE_SUCCESS, props<{ todo: Todo }>());
export const createTodoError = createAction(TodoTypes.CREATE_ERROR, props<{ error: Error }>());
export const updateTodo = createAction(TodoTypes.UPDATE, props<{ id?: number, name: string, date: Date, isDone: boolean }>());
export const updateTodoSuccess = createAction(TodoTypes.UPDATE_SUCCESS, props<{ todo: Todo }>());
export const updateTodoError = createAction(TodoTypes.UPDATE_ERROR, props<{ error: Error }>());
export const deleteTodo = createAction(TodoTypes.DELETE, props<{ id: number }>());
export const deleteTodoSuccess = createAction(TodoTypes.DELETE_SUCCESS, props<{ id: number }>());
export const deleteTodoError = createAction(TodoTypes.DELETE_ERROR, props<{ error: Error }>());
export const displayToast = createAction(TodoTypes.DISPLAY_TOAST, props<{ variant: string, title: string; description: string }>());
