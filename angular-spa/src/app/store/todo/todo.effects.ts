import { Injectable } from '@angular/core';
import { catchError, map, mergeMap, of, switchMap, tap } from 'rxjs';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ToastrService } from 'ngx-toastr';
import * as todoAction from './todo.actions';
import { TodoService } from './todo.service';

@Injectable()
export class TodoEffects {
  constructor(private actions$: Actions,
              private todoService: TodoService,
              private toastr: ToastrService) {}

  public fetchTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoAction.fetchTodo),
      mergeMap(() => this.todoService.fetchTodo()
        .pipe(
          map((todos) =>
            todoAction.fetchTodoSuccess({ todos })),
            catchError((error) => of(
              todoAction.fetchTodoError({ error }),
              todoAction.displayToast({ variant: 'error', title: 'Error', description: 'Oops, we can\'t fetch items.' })
            ))
        ))
    ));

  public createTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoAction.createTodo),
      mergeMap((action) => this.todoService.createTodo(action.name, action.date, action.isDone)
        .pipe(
          switchMap((todo) => [
            todoAction.createTodoSuccess({ todo }),
            todoAction.displayToast({ variant: 'success', title: 'Success', description: 'Create item successfully.' }),
          ]),
          catchError((error) => of(
            todoAction.createTodoError({ error }),
            todoAction.displayToast({ variant: 'error', title: 'Error', description: 'Oops, we can\'t create the item.' })
          ))
        ))
    ));

  public updateTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoAction.updateTodo),
      mergeMap((action) => this.todoService.updateTodo(action.id as number, action.name, action.date, action.isDone)
        .pipe(
          switchMap((todo) => [
            todoAction.updateTodoSuccess({ todo }),
            todoAction.displayToast({ variant: 'success', title: 'Success', description: 'Update item successfully.' }),
          ]),
          catchError((error) => of(
            todoAction.updateTodoError({ error }),
            todoAction.displayToast({ variant: 'error', title: 'Error', description: 'Oops, we can\'t update the item.' })
          ))
        ))
    ));

  public deleteTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoAction.deleteTodo),
      mergeMap((action) => this.todoService.deleteTodo(action.id)
        .pipe(
          switchMap((id) => [
            todoAction.deleteTodoSuccess({ id }),
            todoAction.displayToast({ variant: 'success', title: 'Success', description: 'Delete item successfully.' }),
          ]),
          catchError((error) => of(
            todoAction.deleteTodoError({ error }),
            todoAction.displayToast({ variant: 'error', title: 'Error', description: 'Oops, we can\'t delete the item.' })
          ))
        ))
    ));

  public displayToast$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoAction.displayToast),
      tap((action) => (action.variant !== 'success')
        ? this.toastr.error(action.description, action.title)
        : this.toastr.success(action.description, action.title))
    ), { dispatch: false });
}
