import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Todo } from './todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = environment.apiUrl;
  }

  public fetchTodo(): Observable<Todo[]> {
    return this.http.get<Todo[]>(`${this.baseURL}/api/todos`);
  }

  public createTodo(name: string, date: Date, isDone: boolean): Observable<Todo> {
    const newTodo = { name, date, isDone };
    return this.http.post<Todo>(`${this.baseURL}/api/todos`, newTodo);
  }

  public updateTodo(id: number, name: string, date: Date, isDone: boolean): Observable<Todo> {
    const updateTodo = { name, date, isDone };
    return this.http.put<Todo>(`${this.baseURL}/api/todos/${id}`, updateTodo);
  }

  public deleteTodo(id: number): Observable<number> {
    return this.http.delete<void>(`${this.baseURL}/api/todos/${id}`)
      .pipe(map(() => id));
  }
}
