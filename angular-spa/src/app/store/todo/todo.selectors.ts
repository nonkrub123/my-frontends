import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TodoState, TODO_FEATURE_NAME } from './todo.reducer';

export const selectTodoState = createFeatureSelector<TodoState>(TODO_FEATURE_NAME);

export const selectTodos = createSelector(
  selectTodoState, (state: TodoState) => state.todos
);

export const selectTodo = (id: number) => createSelector(
  selectTodoState, (state: TodoState) => state.todos.find(m => m.id === id)
);

export const selectStatus = createSelector(
  selectTodoState, (state: TodoState) => state.status
);
