import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { counterReducer, CounterState, COUNTER_FEATURE_NAME } from './counter/counter.reducer';
import { weatherReducer, WeatherState, WEATHER_FEATURE_NAME } from './weather/weather.reducer';
import { WeatherEffects } from './weather/weather.effects';
import { todoReducer, TodoState, TODO_FEATURE_NAME } from './todo/todo.reducer';
import { TodoEffects } from './todo/todo.effects';

export interface State {
  [COUNTER_FEATURE_NAME]: CounterState;
  [WEATHER_FEATURE_NAME]: WeatherState;
  [TODO_FEATURE_NAME]: TodoState;
}

export const reducers: ActionReducerMap<State> = {
  [COUNTER_FEATURE_NAME]: counterReducer,
  [WEATHER_FEATURE_NAME]: weatherReducer,
  [TODO_FEATURE_NAME]: todoReducer,
};

export const effects = [
  WeatherEffects,
  TodoEffects,
];

export const metaReducers: MetaReducer<State>[] = [];

export type Status = 'idle' | 'loading' | 'failed';
