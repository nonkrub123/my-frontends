import { createAction, props } from '@ngrx/store';
import { City } from './weather.model';

enum WeatherTypes {
  GET_CITY = '[Weather] Retrieve City',
  GET_CITY_SUCCESS = '[Weather] Retrieve City Success',
  GET_CITY_ERROR = '[Weather] Retrieve City Error',
  DISPLAY_TOAST = '[Weather] Toast Notification',
}

export const getCity = createAction(WeatherTypes.GET_CITY, props<{ cityName: string }>());
export const getCitySuccess = createAction(WeatherTypes.GET_CITY_SUCCESS, props<{ city: City }>());
export const getCityError = createAction(WeatherTypes.GET_CITY_ERROR, props<{ error: Error }>());
export const displayToast = createAction(WeatherTypes.DISPLAY_TOAST, props<{ variant: string, title: string; description: string }>());
