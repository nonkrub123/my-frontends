import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { City } from './weather.model';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private baseURL: string;
  private apiKey: string;

  constructor(private http: HttpClient) {
    this.baseURL = 'https://api.openweathermap.org/data/2.5';
    this.apiKey = environment.apiKey;
  }

  public getCity(cityName: string): Observable<City> {
    return this.http.get<City>(`${this.baseURL}/weather?q=${cityName}&units=metric&appid=${this.apiKey}`);
  }
}
