import { Injectable } from '@angular/core';
import { catchError, map, of, mergeMap, tap } from 'rxjs';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ToastrService } from 'ngx-toastr';
import * as weatherAction from './weather.actions';
import { WeatherService } from './weather.service';

@Injectable()
export class WeatherEffects {
  constructor(private actions$: Actions,
              private weatherService: WeatherService,
              private toastr: ToastrService) {}

  public getCity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(weatherAction.getCity),
      mergeMap((action) => this.weatherService.getCity(action.cityName)
        .pipe(
          map((city) =>
            weatherAction.getCitySuccess({ city })),
            catchError((error) => of(
              weatherAction.getCityError({ error }),
              weatherAction.displayToast({ variant: 'error', title: 'Error', description: 'Oops, we can\'t find that city' })
            ))
        ))
    ));

  public displayError$ = createEffect(() =>
    this.actions$.pipe(
      ofType(weatherAction.displayToast),
      tap((action) => (action.variant !== 'success')
        ? this.toastr.error(action.description, action.title)
        : this.toastr.success(action.description, action.title))
    ), { dispatch: false });
}
