import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeTh from '@angular/common/locales/th';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ToastrModule } from 'ngx-toastr';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppRoutingModule } from './app-routing.module';
import { AppHttpInterceptor } from './app-http.interceptor';
import { reducers, effects } from './store';
import { AppComponent } from './components/app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

registerLocaleData(localeTh);
const localeProvider = { provide: LOCALE_ID, useValue: 'th-TH' };
const interceptorProvider = { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true };
const toastOptions = { positionClass: 'toast-bottom-right', progressBar: true };

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    ToastrModule.forRoot(toastOptions),
    SweetAlert2Module.forRoot(),
  ],
  providers: [
    localeProvider,
    interceptorProvider,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
