import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CounterComponent } from './counter/counter.component';
import { WaveComponent } from './wave/wave.component';


@NgModule({
  declarations: [
    HomeComponent,
    CounterComponent,
    WaveComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
