import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from '@/app/store';
import { selectCounter } from '@/app/store/counter/counter.selectors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  @ViewChild('greeting') private greeting!: ElementRef<HTMLElement>;
  public count$: Observable<number>;

  constructor(private store: Store<State>) {
    this.count$ = this.store.select(selectCounter);
  }

  ngAfterViewInit(): void {
    $(this.greeting.nativeElement).html('Hello Angular!!');
  }
}
