import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from '@/app/store';
import * as counterAction from '@/app/store/counter/counter.actions';
import { selectCounter } from '@/app/store/counter/counter.selectors';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent {
  public count$: Observable<number>;

  constructor(private store: Store<State>) {
    this.count$ = this.store.select(selectCounter);
  }

  incrementClick() {
    this.store.dispatch(counterAction.increment());
  }

  decrementClick() {
    this.store.dispatch(counterAction.decrement());
  }
}
