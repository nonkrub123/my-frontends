import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Observable, Subject, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import type { State, Status } from '@/app/store';
import * as weatherAction from '@/app/store/weather/weather.actions';
import { selectCity, selectStatus } from '@/app/store/weather/weather.selectors';
import type { City } from '@/app/store/weather/weather.model';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit, OnDestroy {
  public city$: Observable<City>;
  public status$: Observable<Status>;
  public subscription: Subscription;
  public city?: City;
  public background?: SafeStyle;
  public search: Subject<string> = new Subject<string>();
  public cityName: string = '';

  constructor(private store: Store<State>,
              private sanitizer: DomSanitizer) {

    this.city$ = this.store.select(selectCity);
    this.subscription = this.city$.subscribe((city) => {
      this.city = city;
      this.cityName = city.name;
      this.setBackground(city);
    });
    this.status$ = this.store.select(selectStatus);
    this.search.subscribe((cityName: string) =>
      this.store.dispatch(weatherAction.getCity({ cityName: cityName })));
  }

  ngOnInit(): void {
    this.setSearch('bangkok');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public setSearch(cityName: string) {
    this.search.next(cityName);
  }

  public goBack(){
    this.setSearch(this.cityName);
  }

  private setBackground(city: City) {
    if (city?.main) {
      const temperature = city?.main.temp ?? 0;
      const bg = (temperature > 0)
        ? (temperature > 10)
          ? (temperature > 20)
            ? '../../../assets/images/summer.jpg'
            : '../../../assets/images/spring.jpg'
          : '../../../assets/images/fall.jpg'
        : '../../../assets/images/winter.jpg';
      this.background = this.sanitizer.bypassSecurityTrustStyle(`url(${bg})`);
    }
  }
}
