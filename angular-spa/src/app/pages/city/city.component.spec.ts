import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { CityComponent } from './city.component';
import { State } from '@/app/store';

describe('CityComponent', () => {
  let component: CityComponent;
  let fixture: ComponentFixture<CityComponent>;
  let mockStore: MockStore<State>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CityComponent ],
      providers: [ provideMockStore() ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(CityComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject(Store) as MockStore<any>;
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    spyOn(component, 'setSearch');
    component.ngOnInit();

    expect(component).toBeTruthy();
    expect(component.setSearch).toHaveBeenCalled();
  }));
});
