import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  public stacks = [
    { group: 'Framework', libraries: ['Angular'] },
    { group: 'Tools', libraries: ['ESLint', 'ngx-env', 'Axios'] },
    { group: 'State Container', libraries: ['RxJS', 'NgRx'] },
    { group: 'Testing', libraries: ['Karma', 'Jasmine'] },
    { group: 'Utilities', libraries: ['Angular Bootstrap', 'jQuery', 'DataTables', 'Toastr', 'SweetAlert2'] },
    { group: 'UI', libraries: ['Bootstrap', 'Sass', 'Font Awesome', 'favicon']}
  ];
}
