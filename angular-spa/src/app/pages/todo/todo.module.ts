import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TodoRoutingModule } from './todo-routing.module';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DataTablesModule } from 'angular-datatables';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { TodoComponent } from './todo.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoTableComponent } from './todo-table/todo-table.component';
import { TodoModalComponent } from './todo-modal/todo-modal.component';


@NgModule({
  declarations: [
    TodoComponent,
    TodoFormComponent,
    TodoTableComponent,
    TodoModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TodoRoutingModule,
    DataTablesModule,
    SweetAlert2Module,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  providers: [BsModalService]
})
export class TodoModule { }
