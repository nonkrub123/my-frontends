import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import type { State, Status } from '@/app/store';
import * as todoAction from '@/app/store/todo/todo.actions';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Todo } from '@/app/store/todo/todo.model';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  @ViewChild('todoModal', { static: false }) private todoModal?: TemplateRef<any>;
  public modalRef?: BsModalRef;
  private options: ModalOptions<any> = { backdrop: 'static', keyboard: false };

  constructor(private modalService: BsModalService,
              private store: Store<State>) {}

  ngOnInit(): void {
    this.store.dispatch(todoAction.fetchTodo());
  }

  openCreateModal(): void {
    if (this.todoModal) {
      this.modalRef =  this.modalService.show(this.todoModal, {
        ...this.options,
        initialState: undefined,
      });
    }
  }

  openEditModal(todo: Todo): void {
    if (this.todoModal) {
      this.modalRef =  this.modalService.show(this.todoModal, {
        ...this.options,
        initialState: todo,
      });
    }
  }
}
