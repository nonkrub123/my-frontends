import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { thLocale } from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { Todo } from '@/app/store/todo/todo.model';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  @Input() public todo?: Todo;
  @Output() public validated = new EventEmitter<Todo>();
  public config: Partial<BsDatepickerConfig>;
  public todoForm: FormGroup;
  public submitted: boolean;

  constructor(private formBuilder: FormBuilder,
    private localeService: BsLocaleService) {

    defineLocale('th', thLocale);
    this.localeService.use('th');
    this.config = Object.assign({}, { containerClass: 'theme-default' });
    this.todoForm = this.formBuilder.group({
      id: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      date: new FormControl(new Date(), [Validators.required]),
      isDone: new FormControl(false),
    });
    this.submitted = false;
  }

  ngOnInit(): void {
    if (this.todo) {
      this.todoForm.patchValue({
        id: this.todo.id,
        name: this.todo.name.trim(),
        date: this.todo.date,
        isDone: this.todo.isDone,
      });
    }
  }

  get name() { return this.todoForm.get('name'); }
  get date() { return this.todoForm.get('date'); }

  validate(): void {
    this.submitted = true;

    if (!this.todoForm.invalid) {
      this.validated.emit(this.todoForm.value);
    }
  }
}
