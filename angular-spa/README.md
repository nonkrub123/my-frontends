# Learn Angular

- การใช้งาน Angular 14 Framework
- การใช้งาน class component
- การตั้งค่า ESlint
- การใช้งาน ngx-env
- การทำ layout
- การทำ routing
- การทำ http client interceptor
- การตั้งค่า angular config
- การตั้งค่า ESlint
- การใช้งาน Bootstrap Style & Bootstrap Script
- การใช้งาน FontAwesome
- การทำ testing ด้วย karma และ jasmine
- การทำ favicon สำหรับทุกอุปกรณ์

### Home

- การใช้งาน รูปภาพ assets สำหรับ img
- การใช้งาน SCSS animation
- การใช้งาน RxJS และ NgRx แบบง่าย ๆ
- การใช้งาน jQuery

### City

- การเรียนใช้ค่า .env config
- การใช้งาน component
- การรับส่งค่าผ่าน component
- การใช้งาน external api
- การใช้งาน RxJS และ NgRx
- การใช้งาน รูปภาพ assets สำหรับ css style
- การจัดการ error
- การใช้งาน toast

### Todo

- การใช้เรียกงาน function ของ child component
- การใช้งาน modal
- การใช้งาน form
- การทำ form validate
- การทำ CRUD ด้วย RxJS และ NgRx
- การใช้งาน toast
- การใช้งาน jQuery DataTables.net
- การใช้งาน SweetAlert2

# คำสั่ง Angular-CLI & Docker เบื้องต้น

- npx --package @angular/cli@latest ng new angular-spa --style=scss
- yarn ng --help
- yarn ng generate module pages/todo --routing
- yarn ng generate component pages/todo --module=todo
- yarn ng generate component pages/todo/todo-table --module=todo
- yarn ng generate component pages/todo/todo-modal --module=todo
- yarn ng generate component pages/todo/todo-form --module=todo
- yarn ng generate service store/todo/todo
- docker-compose up -d [--build]
- docker-compose images
- docker-compose ps
- docker-compose logs
- docker inspect angular-spa
